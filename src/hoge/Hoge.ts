export class Hoge {
  private static instance: Hoge;
  private readonly NAME: string;

  private constructor() {
    this.NAME = 'Hoge';
  }

  static readonly getInstance = (): Hoge => {
    if (!Hoge.instance) {
      Hoge.instance = new Hoge();
    }
    return Hoge.instance;
  }

  readonly getName = (): string => this.NAME;
}
