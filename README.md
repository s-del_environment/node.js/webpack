# Webpack
[公式ドキュメント](https://webpack.js.org/concepts/)


## 導入手順
1. 必要な npm モジュールのインストール  
    `npm install --save-dev webpack webpack-cli ts-loader`
2. 2種類の `webpack.config.js` の作成
    - [開発用](./webpack.config.development.js)
    - [リリース用](./webpack.config.production.js)
3. [`package.json`](./package.json) にビルドスクリプトの設定


## 参考
- [React + TypeScript の環境を整える (1) 自力で webpack 設定する方法｜まくろぐ](https://maku.blog/p/m4dmt3a/)
